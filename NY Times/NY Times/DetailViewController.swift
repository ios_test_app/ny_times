//
//  DetailViewController.swift
//  NY Times
//
//  Created by srinivas gosike on 25/01/20.
//  Copyright © 2020 srinivas gosike. All rights reserved.
//

import UIKit
import WebKit

class DetailViewController: UIViewController {
  @IBOutlet weak var webView: WKWebView!
  @IBOutlet weak var indicator: UIActivityIndicatorView!
  var _url = ""

    override func viewDidLoad() {
        super.viewDidLoad()
      self.title = _url

      let url = NSURL(string: _url)
      let request = NSURLRequest(url: url! as URL)

      webView.navigationDelegate = self
      webView.load(request as URLRequest)


      indicator.center = self.view.center
      indicator.startAnimating()
      indicator.isHidden = false
      indicator.hidesWhenStopped = true
    }

}

extension DetailViewController: WKNavigationDelegate {

  func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
    print("Strat to load")
  }

  func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
    indicator.isHidden = true
    print("finish to load")
    print(_url + "adfadf")
  }

    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
    print(error.localizedDescription)
  }
}
