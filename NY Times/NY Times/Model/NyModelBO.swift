//
//  NyModelBO.swift
//  NY Times
//
//  Created by srinivas gosike on 25/01/20.
//  Copyright © 2020 srinivas gosike. All rights reserved.
//

import UIKit

class NyModelBO: NSObject {

  var title = ""
  var url = ""
  var byline = ""
  var published_date = ""
  var section = ""
}
