//
//  NyTableCell.swift
//  NY Times
//
//  Created by srinivas gosike on 25/01/20.
//  Copyright © 2020 srinivas gosike. All rights reserved.
//

import UIKit

class NyTableCell: UITableViewCell {

  @IBOutlet weak var iv_profile: UIImageView!
  @IBOutlet weak var lbl_title: UILabel!
  @IBOutlet weak var lbl_by: UILabel!
  @IBOutlet weak var lbl_date: UILabel!
  @IBOutlet weak var lbl_section: UILabel!

  override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    MyUtils.makeRounded(view: iv_profile)

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
