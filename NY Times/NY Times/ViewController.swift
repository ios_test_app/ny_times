//
//  ViewController.swift
//  NY Times
//
//  Created by srinivas gosike on 25/01/20.
//  Copyright © 2020 srinivas gosike. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

  @IBOutlet weak var tableView: UITableView!
  let fromDay = "1"
  var nyArray = [NyModelBO]()
  override func viewDidLoad() {
    super.viewDidLoad()

    // Do any additional setup after loading the view, typically from a nib.
    if(isDayValid(day: fromDay)){
      getNyRecords()
    }

  }

  override func viewWillAppear(_ animated: Bool) {
    if let index = self.tableView.indexPathForSelectedRow{
      self.tableView.deselectRow(at: index, animated: true)
    }
  }

  func isDayValid(day: String) -> Bool {
    if(day == "1" || day == "7" || day == "30") {
      return true
    }
    return false
  }

  // API request
  func getNyRecords() {

    self.title = "NY Times Most Popular"
    let apiClient = APIClient(delegate: self);
    let parameters = [
      "":""
    ]
    apiClient.ExecuteRequest(url: "\(Constants.BaseUrl)mostviewed/all-sections/\(fromDay).json?api-key=\(Constants.NY_API_KEY)", parameters: parameters)
  }

  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if (segue.identifier == "nySegueIdentifer") {
      if let vc = segue.destination as? DetailViewController, let detailToSend = sender as? NyModelBO {
        vc._url = detailToSend.url
      }
    }
  }
}

extension ViewController: APIClientDelegate{

  func APIOnSuccess(status: Bool, json: [String : Any], requestCode: Int) {
    if let resultsArray = json["results"] as? [[String: Any]]
    {
      for result in resultsArray
      {
        let bo = NyModelBO()

        bo.title = MyUtils.GetStringFromObject(object: result, key: "title");
        bo.published_date = MyUtils.GetStringFromObject(object: result, key: "published_date");
        bo.section = MyUtils.GetStringFromObject(object: result, key: "section");
        bo.url = MyUtils.GetStringFromObject(object: result, key: "url");
        bo.byline = MyUtils.GetStringFromObject(object: result, key: "byline");

        nyArray.append(bo)
      }
      DispatchQueue.main.async {
        self.tableView.reloadData()
      }
    }
  }

  func APIOnFailure(json: [String : Any], requestCode: Int) {
    print(json)
    if let error = json["fault"] as? [String: Any] {
      let message = error["faultstring"] as? String

      let alert = UIAlertController(title: "Error", message: message, preferredStyle: UIAlertController.Style.alert)
      alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
      self.present(alert, animated: true, completion: nil)
    }
  }

  func APIOnError(requestCode: Int) {
    print("Error")
  }
}

extension ViewController: UITableViewDataSource, UITableViewDelegate {

  // TableView DataSource and Delegate methods
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return 140;
  }

  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return nyArray.count;
  }

  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "NyTableCell", for: indexPath) as! NyTableCell

    let bo = nyArray[indexPath.row]
    cell.lbl_title.text = bo.title
    cell.lbl_by.text = bo.byline
    cell.lbl_date.text = bo.published_date
    cell.lbl_section.text = bo.section
    return cell;
  }

  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    let bo = nyArray[indexPath.row]
    performSegue(withIdentifier: "nySegueIdentifer", sender: bo)
  }
}

