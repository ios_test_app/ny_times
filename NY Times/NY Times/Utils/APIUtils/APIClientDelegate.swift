//
//  APIClientDelegate.swift
//  NY Times
//
//  Created by srinivas gosike on 25/01/20.
//  Copyright © 2020 srinivas gosike. All rights reserved.
//

protocol APIClientDelegate {
    func APIOnSuccess(status: Bool, json: [String:Any], requestCode: Int);
    func APIOnFailure(json: [String:Any], requestCode: Int);
    func APIOnError(requestCode: Int);
}
