//
//  APIClient.swift
//  NY Times
//
//  Created by srinivas gosike on 25/01/20.
//  Copyright © 2020 srinivas gosike. All rights reserved.
//

import UIKit

class APIClient {
    
    public var actyIndicator = ActyIndicator();
    var delegate: APIClientDelegate!
    var urlEx = ""
    var headerEx = [String: String]()
    var parametersEx = [String: Any] ()
    public init(delegate: APIClientDelegate)
    {
        self.delegate = delegate;
    }

    public func ExecuteRequest(url: String, parameters: [String: Any], showIndicator: Bool = false, view: UIView? = nil, requestCode: Int = 0)
    {
      var status = false
      let url = URL(string: url)!

      let task = URLSession.shared.dataTask(with: url) {(data, response, error) in
        guard let data = data else { return }

        do {
          //create json object from data
          if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
            if let st = json["status"] as? String
            {
              status = st == "OK" ? true : false;
            }

            if (json["results"] as? [[String: Any]]) != nil
            {
              self.delegate.APIOnSuccess(status: status, json: json, requestCode: requestCode);
            }
            else
            {
              self.delegate.APIOnFailure(json: json, requestCode: requestCode);
            }
          }
        } catch let error {
          print(error)
          self.delegate.APIOnError(requestCode: requestCode)
        }
      }
      task.resume()
  }
}
