//
//  MyUtils.swift
//  NY Times
//
//  Created by srinivas gosike on 25/01/20.
//  Copyright © 2020 srinivas gosike. All rights reserved.
//

import UIKit

class MyUtils {

  public static func GetStringFromObject(object: [String:Any], key: String) -> String
  {
    var value = "";
    if let str = object[key] as? String
    {
      value = str;
    }
    return value;
  }

  public static func makeRounded(view: UIView) {
    view.layer.cornerRadius = view.frame.size.width/2
    view.clipsToBounds = true
  }

}
