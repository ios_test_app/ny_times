//
//  NY_TimesTests.swift
//  NY TimesTests
//
//  Created by srinivas gosike on 25/01/20.
//  Copyright © 2020 srinivas gosike. All rights reserved.
//

import XCTest
@testable import NY_Times

class NY_TimesTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

  func testIsDayValid() {
    let vc = ViewController()
    XCTAssertTrue(vc.isDayValid(day: "1"))
    XCTAssertTrue(vc.isDayValid(day: "7"))
    XCTAssertTrue(vc.isDayValid(day: "30"))

  }

    func testExample() {
          // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
