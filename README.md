NY Times Most Popular Articles

Configuration: Xcode 10.1
                       Deployment target ios 10.0 and above
                      
It's a simple app to hit the NY Times Most Popular Articles API and show a list of articles, that shows details when items on the list are tapped (a typical master/detail app).

API is used for (http://api.nytimes.com/svc/mostpopular/v2/mostviewed/all-sections/1.json?api-key={sample key})
sample key is "SteWWgNnMuu2TTCkv82yJAIdlVGCF7FG"

Utils:

APIUtils:-
API request is done by URLSession class and related classes provide an API for downloading data from and uploading data to endpoints indicated by URLs. This app can also use this API to perform background downloads when this app isn’t running or, in iOS, while this app is suspended.

MyUtils:-
For special conversions and validations MyUtils.swift class will usefull 

To show the Popular Articles list we used Tableview 
Tables are commonly used by apps whose data is highly structured or organized hierarchically. Apps that contain hierarchical data often use tables in conjunction with a navigation view controller, which facilitates navigation between different levels of the hierarchy. 

To show the detail used WebView
WebView is the core view class in the WebKit framework that manages interactions between the WebFrame and WebFrameView classes. To embed web content in your application, you just create a WebView object, attach it to a window, and send a load(_:) message to its main frame.

Unit Tests:
A signle unit test case is there to validate the fromDay for a API call to test this API, you can use all-sections for the section path component in the URL, and 7 for period (available period values are 1, 7 and 30, which represents how far back, in days, the API returns results for).

